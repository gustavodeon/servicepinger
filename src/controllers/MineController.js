const { getStatus } = require('mc-server-status')

async function index(req, res) {
  const data = ({ host: url, port: port, version: version } = req.params)

  console.log(data)

  try {
    const status = await getStatus(data.url)
    const temp = {
      motd: status.description.text,
      ping: status.ping,
    }
    console.log(temp)
    res.status(200)
    res.json({
      status: 'online',
    })
  } catch (error) {
    console.log(error)
    res.status(401)
    res.json({
      status: 'offline',
    })
  }
}

module.exports = { index }
