FROM node:16.14-alpine

COPY ./ /home/node/app/

WORKDIR /home/node/app/

ENV NODE_ENV=PRODUCTION

RUN "yarn" "install"

CMD "yarn" "prod"